#ifndef DATAHANDLER_HH_
#define DATAHANDLER_HH_


class Analysis;

class DataHandler{

public:
  static DataHandler* Instance(void);
  ~DataHandler();

  TObject* Get(TString);
  void RegisterAnalysis(Analysis *a){ fAnaVec.push_back(a); }

private:
  vector<Analysis*> fAnaVec;

}


#endif DATAHANDLER_HH_
