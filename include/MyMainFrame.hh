#ifndef MYMAINFRAME_HH_
#define MYMAINFRAME_HH_

#include <TGFrame.h>
#include <TGTab.h>
#include <TGButton.h>
#include <TGListBox.h>
#include <TGComboBox.h>
#include <TGTextEntry.h>
#include <TGNumberEntry.h>

class TGWindow;
class TRootEmbeddedCanvas;

class MyMainFrame : public TGMainFrame{

private:
  TRootEmbeddedCanvas *fEcanvas;
public:
  MyMainFrame(const TGWindow *p,UInt_t w,UInt_t h);
  virtual ~MyMainFrame();
  void DoTab(Int_t id);
  void DoStart();
  void DoPause();
  void DoStop();
  void DoCancel();
  void DoSetFileName();
  void DoLoadConfig();
  void DoSaveConfig();
  void DoSetupCanvas();
  void DoCreateCanvas();
  void DoCloseCanvasWindow();
  void DoUpdateEquation();
  void CloseWindow();


private:
  TGTab             *fTab;
  TGCompositeFrame  *fMain, *fF3, *fF4, *fF5, *fF6, *fF7, *fFcanv;
  TGGroupFrame      *fF1, *fF2, *fCanvPad[100];
  TGTransientFrame  *fCanvFrame;
  TGLayoutHints     *fL1, *fL2, *fL3, *fL4, *fL5, *fL6, *fL7, *fL8, *fL9;
  TGTextEntry       *fTxt1, *fTxt2, *fParamBox, *fFormulaBox, *fDrawOpt[100];
  TGComboBox        *fPlotList[300];
  TGNumberEntry     *fRunNumEntry, *fCanvNrow, *fCanvNcol;
  TGCompositeFrame  *fCanvasTab;
  TGButton          *fStartB, *fPauseB, *fStopB, *fCloseB, *fFileSetB, *fLoadConfigB, *fSaveConfigB, *fNewCanvasB, *fCreateCanvasB, *fCancelCanvasB;

  ClassDef(MyMainFrame,0);
};


#endif /* MYMAINFRAME_HH_ */
