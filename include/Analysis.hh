#ifndef ANALYSIS_HH_
#define ANALYSIS_HH_

#include <TFormula.h>
#include <TString.h>
#include <TGraph.h>

#include <vector>

//Probably don't need this since users won't use these
//variables
enum EOperation{
  integral = 0;
  getMean  = 1;
  getRMS   = 2;
  getMin   = 3;
  getMax   = 4;
  eval     = 5;
}

class Analysis{

public:
  Analysis(TString, TString);
  Analysis(TGraph*, int, double, double);
  virtual ~Analysis();

  void Eval();
  bool IsValid();
  double GetValPtr();
  TString GetName(){ return fName; }

private:
  TString  fName;
  TFormula *fFormula;
  double   fValue;
  TGraph   *fGraph;
  int      fOperation;
  double   fMin;
  double   fMax;
  bool     VALID;
  vector< int* > fIParams;
  vector< double* > fDParams;
  vector< bool > fParamType; // 0 for int 1 for double maybe use ints?

}



#endif /* ANALYSIS_HH_ */
