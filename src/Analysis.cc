#include "Analysis.hh"
#include "DataHandler.hh"


#include <TDirectory.h>


/*
 * TFormula constructor
 */
Analysis::Analysis(TString name, TString formula):
  fName(name)
{

  fFormula = new TFormula( name.Data(), formula.Data() );

  TString type;
  void* data;
  DataHandler *dh = DataHandler::Instance();
  for( int i = 0; i < fFormula->GetNpar(); i++ ){
    data = dh->Get( fFormula->GetParName(i), type );

    // If the DataHandler can't find the parameter
    // the Analysis is not valid
    if( data == NULL ){
      VALID = false;
      return;
    }

    else if(type == "I"){
      fParamType.push_back(0);
      fIParams.push_back(data);
      fDParams.push_back(NULL); //Push a null to keep the vectors the same length
    }else if(type == "D"){
      fParamType.push_back(1);
      fDParams.push_back(data);
      fIParams.push_back(NULL);
    }

  }
}

/*
 * TGraph constructor
 */
Analysis::Analysis(TGraph *g, int op, double min, double max):
  fFormula(NULL),
  fGraph(g),
  fOperation(op),
  fMin(min),
  fMax(max)
{

}

/*
 *  Evaluate for the current event
 */
Analysis::~Analysis(){

  if(fFormula != NULL) delete fFormula;

}


/*
 *  Evaluate for the current event
 */
void Analysis::Eval(){

}
