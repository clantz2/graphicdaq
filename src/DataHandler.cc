#include "DataHandler.hh"

DataHandler* DataHandler::DataHandler = NULL;

DataHandler* DataHandler::Instance(void)
{
    if (DataHandler == NULL) {
        DataHandler = new DataHandler();
    }
    return DataHandler;
}


/*
 * Destructor
 */
DataHandler::~DataHandler(){

}

/*
 * Get the requested Analysis or raw data point
 * and set value of type to indicate data type
 * 'I' for int and 'D' for double
 */
void* DataHandler::Get(TString s, TString &type){

  for( Analysis ana : fAnaVec ){
    if( s == ana->GetName() ){
      type = "D";
      return ana->GetValPtr();
    }
  }

  //Repeat something similar when devices are added
  // for(devices){
  //   for(channels){
  //     do it
  //   }
  // }

  return NULL;
}
