#include "MyMainFrame.hh"

#include <TGClient.h>
#include <TCanvas.h>

#include <TGLabel.h>
#include <TGMsgBox.h>
#include <TF1.h>
#include <TRandom.h>
#include <TRootEmbeddedCanvas.h>
#include <TApplication.h>

using namespace std;

/*
 * Constructor
*/
MyMainFrame::MyMainFrame(const TGWindow *p,UInt_t w,UInt_t h)
  : TGMainFrame(p,w,h) {

  SetCleanup(kDeepCleanup);

  //Make the X button close the application
  Connect("CloseWindow()", "MyMainFrame", this, "CloseWindow()");

  fMain = new TGHorizontalFrame(this, 60, 20, kFixedWidth);

  fStartB = new TGTextButton(fMain, "&Start");
  fStartB->Connect("Clicked()", "MyMainFrame", this, "DoStart()");
  fPauseB = new TGTextButton(fMain, "&Pause");
  fPauseB->Connect("Clicked()", "MyMainFrame", this, "DoPause()");
  fStopB = new TGTextButton(fMain, "&Stop");
  fStopB->Connect("Clicked()", "MyMainFrame", this, "DoStop()");
  fCloseB = new TGTextButton(fMain, "&Close");
  fCloseB->Connect("Clicked()", "MyMainFrame", this, "CloseWindow()");


  // Make all of the hints here to keep from making too many similar hints
  // Avaiable hints are:
  // kLHintsNoHints, kLHintsLeft, kLHintsCenterX, kLHintsRight, kLHintsTop,
  // kLHintsCenterY, kLHintsBottom, kLHintsExpandX, kLHintsExpandY, kLHintsNormal

  fL1 = new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX,
                          2, 2, 2, 2);
  fL2 = new TGLayoutHints(kLHintsBottom | kLHintsRight, 2, 2, 5, 1);
  fL3 = new TGLayoutHints(kLHintsTop | kLHintsLeft, 5, 5, 5, 5);
  fL4 = new TGLayoutHints(kLHintsBottom | kLHintsLeft, 2, 2, 2, 2);
  fL5 = new TGLayoutHints(kLHintsBottom | kLHintsExpandX | kLHintsExpandY, 2, 2, 5, 1);
  fL6 = new TGLayoutHints(kLHintsCenterX | kLHintsCenterY, 3, 3, 3, 3);
  fL7 = new TGLayoutHints(kLHintsCenterX | kLHintsCenterY, 20, 20, 10, 10);
  fL8 = new TGLayoutHints(kLHintsExpandX | kLHintsExpandY, 20, 20, 10, 10);
  fL9 = new TGLayoutHints(kLHintsCenterY | kLHintsRight, 20, 20, 10, 10);

  fMain->AddFrame(fStartB, fL1);
  fMain->AddFrame(fPauseB, fL1);
  fMain->AddFrame(fStopB, fL1);
  fMain->AddFrame(fCloseB, fL1);

  fMain->Resize(150, fStartB->GetDefaultHeight());
  AddFrame(fMain, fL2);


  // Create the tab object (contains multiple tabs)
  fTab = new TGTab(this, w, h);
  fTab->Connect("Selected(Int_t)", "MyMainFrame", this, "DoTab(Int_t)");


  /****************************************
  * Main Tab
  ****************************************/
  TGCompositeFrame *tf = fTab->AddTab("Main");
  tf->SetLayoutManager(new TGVerticalLayout(tf));

  //Save name
  //
  fF1 = new TGGroupFrame(tf, "Save File Name", kHorizontalFrame);
  fF1->SetTitlePos(TGGroupFrame::kLeft);
  tf->AddFrame(fF1, fL3);
  fF1->SetLayoutManager(new TGHorizontalLayout(fF1));


  fF1->AddFrame(new TGLabel(fF1, new TGHotString("Name:")), fL4);
  fF1->AddFrame(fTxt1 = new TGTextEntry(fF1, new TGTextBuffer(100)), fL4);

  fF1->AddFrame(new TGLabel(fF1, new TGHotString("Start Number:")), fL4);
  fF1->AddFrame(fRunNumEntry = new TGNumberEntry(fF1, 0, 12, 1), fL4);

  fF1->AddFrame(fFileSetB = new TGTextButton(fF1, "&Set"), fL4 );
  fFileSetB->Connect("Clicked()", "MyMainFrame", this, "DoSetFileName()");
  fFileSetB->Resize(100, fFileSetB->GetDefaultHeight());

  fTxt1->Resize(150, fTxt1->GetDefaultHeight());
  fRunNumEntry->Resize(75, fRunNumEntry->GetDefaultHeight());

  //Config File
  //
  fF2 = new TGGroupFrame(tf, "Config file", kHorizontalFrame);
  fF2->SetTitlePos(TGGroupFrame::kLeft);
  fL3 = new TGLayoutHints(kLHintsTop | kLHintsLeft, 5, 5, 5, 5);
  tf->AddFrame(fF2, fL3);
  fF2->SetLayoutManager(new TGHorizontalLayout(fF2));

  fF2->AddFrame(fLoadConfigB = new TGTextButton(fF2, "&Load"), fL4 );
  fLoadConfigB->Connect("Clicked()", "MyMainFrame", this, "DoLoadConfig()");

  fF2->AddFrame(fSaveConfigB = new TGTextButton(fF2, "&Save"), fL4 );
  fSaveConfigB->Connect("Clicked()", "MyMainFrame", this, "DoSaveConfig()");


  /****************************************
  * Config Tab
  ****************************************/
  tf = fTab->AddTab("Config");
  tf->SetLayoutManager(new TGVerticalLayout(tf));
  fF3 = new TGCompositeFrame(tf, 60, 20, kVerticalFrame);
  tf->AddFrame(fF3, fL3);


  /****************************************
  * Canvas Tab
  ****************************************/
  fCanvasTab = tf = fTab->AddTab("Canvas");
  tf->SetLayoutManager(new TGVerticalLayout(tf));

  fF4 = new TGCompositeFrame(tf, 60, 20, kHorizontalFrame);

  TGNumberFormat::EStyle style = TGNumberFormat::kNESInteger;
  TGNumberFormat::EAttribute attr = TGNumberFormat::kNEAPositive;
  TGNumberFormat::ELimit lim;

  fF4->AddFrame(new TGLabel(fF4, new TGHotString("nRows:")), fL3);
  fCanvNcol = new TGNumberEntry(fF4, 1, 12);
  fCanvNcol->SetFormat( style, attr );
  fCanvNcol->SetLimitValues( 1, 10 );
  fF4->AddFrame(fCanvNcol, fL3 );

  fF4->AddFrame(new TGLabel(fF4, new TGHotString("nColumns:")), fL3);
  fCanvNrow = new TGNumberEntry(fF4, 1, 12);
  fCanvNrow->SetFormat( style, attr );
  fCanvNrow->SetLimitValues( 1, 10 );
  fF4->AddFrame(fCanvNrow, fL3);

  fF4->AddFrame(fNewCanvasB = new TGTextButton(fF4, "&Assign Pads"), fL3 );
  fNewCanvasB->Connect("Clicked()", "MyMainFrame", this, "DoSetupCanvas()");

  tf->AddFrame(fF4, fL6);


  /****************************************
  * Analysis Tab
  ****************************************/
  tf = fTab->AddTab("Analysis");
  tf->SetLayoutManager(new TGVerticalLayout(tf));

  fF5 = new TGCompositeFrame(tf, 60, 20, kVerticalFrame);

  fF6 = new TGCompositeFrame(fF5, 60, 20, kHorizontalFrame);
  fF6->AddFrame(new TGLabel(fF5, new TGHotString("New Parameter Name")), fL3);
  fF6->AddFrame(new TGLabel(fF5, new TGHotString("Formula")), fL3);
  fF5->AddFrame(fF6,fL3);


  fF7 = new TGCompositeFrame(fF5, 60, 20, kHorizontalFrame);
  fF7->AddFrame(fParamBox = new TGTextEntry(fF7, new TGTextBuffer(100)), fL1);
  fF7->AddFrame(fFormulaBox = new TGTextEntry(fF7, new TGTextBuffer(100)), fL6);
  fParamBox->Resize(125, fParamBox->GetDefaultHeight());
  fFormulaBox->Resize(300, fFormulaBox->GetDefaultHeight());

  fF5->AddFrame(fF7,fL3);

  tf->AddFrame(fF5, fL6);




  //Add the tab frame to the mainframe
  AddFrame(fTab, fL5);


  // Sets window name and shows the main frame
  SetWindowName("Graphic DAQ");
  Resize(w,h);
  MapWindow();
  MapSubwindows();
}

/*
 * Destructor
 * Clean up used widgets: frames, buttons, layout hints
 */
MyMainFrame::~MyMainFrame() {
  // Clean up used widgets: frames, buttons, layout hints
  Cleanup();
}

/*
 *  Do nothing. Do I need to have a tab signal assigned to a slot?
 *  Maybe I'll need it to update avaialble data
 */
void MyMainFrame::DoTab(Int_t id)
{

}

/*
 *  Start data taking
 */
void MyMainFrame::DoStart()
{
   cout << "You pressed Start. This doesn't do anything yet" << endl;
}

/*
 *  Stop data taking, but do not close the output file
 */
void MyMainFrame::DoPause()
{
   cout << "You pressed Pause. This doesn't do anything yet" << endl;
}

/*
 *  Stop data taking and save the file
 */
void MyMainFrame::DoStop()
{
   cout << "You pressed Stop. This doesn't do anything yet" << endl;
}

/*
 *  Stop data taking and save the file
 */
void MyMainFrame::DoSetFileName()
{
   cout << "You pressed Set. This doesn't do anything yet" << endl;
}

/*
 *  Stop data taking and save the file
 */
void MyMainFrame::DoLoadConfig()
{
   cout << "You pressed Load. This doesn't do anything yet" << endl;

   //Add a file browser. The example below is from the root forums
   //I would want to use a TGTransientFrame (I think)
   /*
   TGMainFrame *pMyMainFrame = new TGMainFrame(gClient->GetDefaultRoot(), 300, 600);
   pMyMainFrame->MapWindow();
   pMyMainFrame->SetEditable();
   TGFileBrowser *pBrowser = new TGFileBrowser(pMyMainFrame);
   pMyMainFrame->SetEditable(kFALSE);
   pBrowser->AddFSDirectory("/", “/”);
   pBrowser->GotoDir(gSystem->pwd());
   pMyMainFrame->MapSubwindows();
   pMyMainFrame->Layout();
   */
}

/*
 *  Stop data taking and save the file
 */
void MyMainFrame::DoSaveConfig()
{
   cout << "You pressed Save. This doesn't do anything yet" << endl;
}

/*
 *  Stop data taking and save the file
 */
void MyMainFrame::DoUpdateEquation()
{
   cout << "You pressed Save. This doesn't do anything yet" << endl;
}

/*
 *  Set canvas dimensions and add list boxes accordingly
 *  Might get replaced with less dynamic behavior if necessary
 */
void MyMainFrame::DoSetupCanvas()
{

  int nRow = fCanvNrow->GetNumber();
  int nCol = fCanvNcol->GetNumber();
  if(nRow > 10 || nCol > 10){
    // Disconnect CloseWindow from the main frame so you can't close
    // until you acknowledge the error
    Disconnect("CloseWindow()");

    new TGMsgBox(gClient->GetRoot(), fMain, "Too Many Pads!!!",
                 "That's a lot of pads you're asking for there, bud",
                 kMBIconExclamation, kMBOk);

    //Reconnect the close window button once the error is acknowledged
    Connect("CloseWindow()", "MyMainFrame", this, "CloseWindow()");

    if(nRow > 10) fCanvNrow->SetNumber(10);
    if(nCol > 10) fCanvNcol->SetNumber(10);
    return;
  }

  fCanvFrame = new TGTransientFrame(gClient->GetRoot(), fMain, nCol*200, nRow*125);
  fCanvFrame->Connect("CloseWindow()", "MyMainFrame", this, "DoCloseCanvasWindow()");
  fCanvFrame->DontCallClose(); // to avoid double deletions.
  fCanvFrame->SetCleanup(kDeepCleanup);


  fFcanv = new TGCompositeFrame(fCanvFrame, 60, 20, kVerticalFrame);


  /****************************************
  * Pad matrix
  ****************************************/
  TGGroupFrame *cMatrix = new TGGroupFrame(fFcanv, "Canvas", kVerticalFrame);
  cMatrix->SetTitlePos(TGGroupFrame::kCenter);
  fFcanv->AddFrame(cMatrix, fL8);
  cMatrix->SetLayoutManager(new TGMatrixLayout(cMatrix, nCol, nRow, 20));


  for(int i = 0; i < nRow*nCol; i++ ){

    //New Pad
    fCanvPad[i] = new TGGroupFrame(cMatrix, Form("Pad%d",i+1), kHorizontalFrame);
    fCanvPad[i]->SetTitlePos(TGGroupFrame::kCenter);
    cMatrix->AddFrame(fCanvPad[i], fL8);
    fCanvPad[i]->SetLayoutManager(new TGMatrixLayout(fCanvPad[i], 4, 2, 10));

    //X
    fCanvPad[i]->AddFrame(new TGLabel(fCanvPad[i], new TGHotString("X:")), fL9);
    fPlotList[3*i+0] = new TGComboBox( fCanvPad[i], i+0 );
    fCanvPad[i]->AddFrame(fPlotList[3*i + 0], fL6);

    //Y
    fCanvPad[i]->AddFrame(new TGLabel(fCanvPad[i], new TGHotString("Y:")), fL9);
    fPlotList[3*i+1] = new TGComboBox( fCanvPad[i], i+1 );
    fCanvPad[i]->AddFrame(fPlotList[3*i + 1], fL6);

    //Z
    fCanvPad[i]->AddFrame(new TGLabel(fCanvPad[i], new TGHotString("Z:")), fL9);
    fPlotList[3*i+2] = new TGComboBox( fCanvPad[i], i+2 );
    fCanvPad[i]->AddFrame(fPlotList[3*i + 2], fL6);

    //Draw option
    fCanvPad[i]->AddFrame(new TGLabel(fCanvPad[i], new TGHotString("Option:")), fL6);
    fCanvPad[i]->AddFrame(fDrawOpt[i] = new TGTextEntry(fCanvPad[i], new TGTextBuffer(100)), fL6);


    //TODO: Replace this with available data from DataHandler
    for(int j = 0; j < 5; j++){
      fPlotList[3*i+0]->AddEntry(Form("Data%d",j),j);
      fPlotList[3*i+1]->AddEntry(Form("Data%d",j),j);
      fPlotList[3*i+2]->AddEntry(Form("Data%d",j),j);
    }


    fPlotList[3*i+0]->Resize(75,20);
    fPlotList[3*i+1]->Resize(75,20);
    fPlotList[3*i+2]->Resize(75,20);
    fDrawOpt[i]->Resize(75,20);

    fCanvPad[i]->Resize();
  }
  cMatrix->Resize();



  /****************************************
  * Create and Cancel buttons
  ****************************************/

  TGHorizontalFrame *buttonFrame = new TGHorizontalFrame(fFcanv, 60, 20, kFixedWidth);

  fCreateCanvasB = new TGTextButton(buttonFrame, "&Create");
  fCreateCanvasB->Connect("Clicked()", "MyMainFrame", this, "DoCreateCanvas()");

  fCancelCanvasB = new TGTextButton(buttonFrame, "&Cancel");
  fCancelCanvasB->Connect("Clicked()", "MyMainFrame", this, "DoCloseCanvasWindow()");

  buttonFrame->AddFrame(fCreateCanvasB, fL7);
  buttonFrame->AddFrame(fCancelCanvasB, fL7);


  fFcanv->AddFrame(buttonFrame, fL1);

  fFcanv->Resize();

  fCanvFrame->AddFrame(fFcanv,fL8);

  fCanvFrame->MapSubwindows();
  fCanvFrame->Resize();
  // position relative to the parent's window
  fCanvFrame->CenterOnParent();
  fCanvFrame->SetWindowName("New Canvas");
  fCanvFrame->MapWindow();
  gClient->WaitFor(fCanvFrame);

}

/*
 *  Close the new canvas frame
 */
void MyMainFrame::DoCreateCanvas()
{
   cout << "You clicked Create, this doesn't do anything yet" << endl;
}

/*
 *  Close the new canvas frame
 */
void MyMainFrame::DoCloseCanvasWindow()
{
  fCanvFrame->DeleteWindow();
}

/*
 *  Got close message for this MainFrame. Terminates the application.
 */
void MyMainFrame::CloseWindow()
{
   gApplication->Terminate();
}
