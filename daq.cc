#include "MyMainFrame.hh"
#include "TApplication.h"
#include "TROOT.h"
#include "TGClient.h"

int main(int argc, char** argv){
  TApplication theApp("App",&argc,argv);
  new MyMainFrame(gClient->GetRoot(),600,400);
  theApp.Run();
  return 0;
}
