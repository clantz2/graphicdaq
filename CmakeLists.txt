#Basic Cmake structure created by Chad Lantz on 25/03/2021
project( GraphicDAQ )
cmake_minimum_required(VERSION 2.8)

#Colors definition for messages
if(NOT WIN32)
  string(ASCII 27 Esc)
  set(ColourReset "${Esc}[m")
  set(ColourBold  "${Esc}[1m")
  set(Red         "${Esc}[31m")
  set(Green       "${Esc}[32m")
  set(Yellow      "${Esc}[33m")
  set(Blue        "${Esc}[34m")
  set(White       "${Esc}[37m")
  set(BoldRed     "${Esc}[1;31m")
  set(BoldGreen   "${Esc}[1;32m")
  set(BoldYellow  "${Esc}[1;33m")
  set(BoldBlue    "${Esc}[1;34m")
  set(BoldWhite   "${Esc}[1;37m")
endif()

message("${Green}
     _/_/_/                                              _/_/_/      _/_/      _/_/
  _/          _/_/      _/_/      _/_/    _/    _/      _/    _/  _/    _/  _/    _/
 _/  _/_/  _/    _/  _/    _/  _/_/_/_/  _/    _/      _/    _/  _/_/_/_/  _/  _/_/
_/    _/  _/    _/  _/    _/  _/        _/    _/      _/    _/  _/    _/  _/    _/
 _/_/_/    _/_/      _/_/      _/_/_/    _/_/_/      _/_/_/    _/    _/    _/_/  _/
                                            _/
                                       _/_/
${ColourReset}")


set (CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${PROJECT_SOURCE_DIR}/cmakeModules/)
#----------------------------------------------------------------------------
# Find ROOT
find_package (ROOT REQUIRED COMPONENTS Gui Spectrum)
include_directories (${ROOT_INCLUDE_DIR})
include(${ROOT_USE_FILE})
#----------------------------------------------------------------------------
# Find Xerces-C
#Xerces-C support
find_package (Xerces REQUIRED)
include_directories (${XERCESC_INCLUDE})
#----------------------------------------------------------------------------
# Locate sources and headers for this project
include_directories(${PROJECT_SOURCE_DIR}/include)
file(GLOB sources ${PROJECT_SOURCE_DIR}/src/*.cc)
file(GLOB headers ${PROJECT_SOURCE_DIR}/include/*.hh)
#----------------------------------------------------------------------------
#Create the mainframe dictionary necessary for signal/slot behavioir
include_directories( ${PROJECT_SOURCE_DIR}/include ${ROOT_INCLUDE_DIRS} )
ROOT_GENERATE_DICTIONARY( G__mainFrameDict MyMainFrame.hh LINKDEF include/LinkDef.hh )
add_library ( mainFrameDict SHARED src/MyMainFrame.cc G__mainFrameDict.cxx )
target_link_libraries ( mainFrameDict ${ROOT_LIBRARIES} ${ROOT_COMPONENT_LIBRARIES})
#----------------------------------------------------------------------------
# Add the executable, and link it to the root and xerces libraries and mainframe dictionary
SET(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")
add_executable(GraphicDAQ  daq.cc)
target_link_libraries( GraphicDAQ mainFrameDict )
#----------------------------------------------------------------------------
#Install the mainframe dictionary and library
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/libmainFrameDict_rdict.pcm DESTINATION ${CMAKE_INSTALL_PREFIX}/lib)
install(TARGETS mainFrameDict LIBRARY DESTINATION lib)
#----------------------------------------------------------------------------
# Install the excutable
install(TARGETS GraphicDAQ RUNTIME DESTINATION bin)
